#!/usr/bin/make -f

include /usr/share/dpkg/buildflags.mk

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# To correctly build doc/ with autosetup.
# See https://github.com/neomutt/neomutt/issues/980.
EXTRA_CFLAGS_FOR_BUILD:=$(shell dpkg-buildflags --get CFLAGS)
EXTRA_LDFLAGS_FOR_BUILD:=$(shell dpkg-buildflags --get LDFLAGS)

no_notmuch_architectures := alpha hppa hurd-i386 powerpcspe ppc64 sh4 sparc64
ifneq ($(DEB_HOST_ARCH),$(filter $(DEB_HOST_ARCH), $(no_notmuch_architectures)))
	notmuch = --notmuch
else
	notmuch = --disable-notmuch
endif

%:
	dh $@ --builddirectory

override_dh_autoreconf:

override_dh_auto_configure:
	dh_auto_configure --  \
		--mandir=/usr/share/man \
		--libexecdir=/usr/libexec \
		--with-mailpath=/var/mail \
		--gpgme	\
		--lua \
		$(notmuch) \
		--with-ui \
		--gnutls \
		--gss \
		--idn \
		--mixmaster \
		--sasl \
		--tokyocabinet \
		--sqlite \
		--autocrypt

override_dh_auto_test:

override_dh_auto_install:
	dh_auto_install --destdir=debian/tmp
	
override_dh_install:
	cd debian/tmp/usr/share/doc/neomutt && \
	rm -rf samples/iconv samples/ca-bundle.crt
	
	chmod +x debian/extra/lib/*
	
	( sed -e '/## More settings/,$$d' debian/tmp/etc/neomuttrc || exit 1 ; \
	  cat debian/extra/rc/neomuttrc.foot ) > debian/tmp/neomuttrc
	
	dh_install

override_dh_installchangelogs:
	dh_installchangelogs ChangeLog.md

